package com.enrichment.G.method_overloading;

public class StartHereForMethodOverloading {
    /* Method overloading is a nifty trick Java has that allows us to reuse method names. Check out these two methods */

    public int addNumbers(int a, int b) {
        return a + b;
    }

    public int addNumbers(int a, int b, int c) {
        return a + b + c;
    }

    /* Yay! We don't have any compiler errors. As long as the method signatures are different, we can use the same
     * method names over and over, look at these too */

    public double addNumbers(double a, double b) {
        return a + b;
    }

    /* Even though this addNumbers has the same amount of arguments as the one on line 6, it still is seen as a different
     * method because it uses doubles as arguments instead of integers. Now consider what would happen if we uncomment
     * the method below. */

//    public static int addNumbers(double a, double b) {
//        return (int)(a + b);
//    }

    /* What is the difference between the above method and the one on line 17? Just changing the return type isn't
     * enough for us to use method overloading. The secret to overloading is with the method parameters! To overload a
     * method the parameters must be different in number, or different in type!
     *
     * Method overloading allows us to support different permutations of arguments being passed to our methods, in other
     * words, it prevents the following from happening in code */

    public int addTwoNumbers(int a, int b) {
        return a + b;
    }

    public int addThreeNumbers(int a, int b, int c) {
        return a + b + c;
    }

    /* Or even worse, consider this */

    /* With overloading */
    public void castMagicSpellOn(Wizard wizard) {
        System.out.println("You smite the wizard!");
        System.out.println("The wizard says " + wizard.getFinalWords());
    }

    public void castMagicSpellOn(Witch witch) {
        System.out.println("You transmute the witch into a butterfly!");
        System.out.println("She curses you! " + witch.getRetalationCurse());
    }

    public void castMagicSpellOn(Warlock warlock) {
        System.out.println("You banish the warlock to another realm");
        System.out.println(warlock.getFadingScream());
    }

    public void castMagicSpellOn(Warblegarblewarble warblegarblewarble) {
        System.out.println("What is this thing? You cast a spell and nothing happened?");
        System.out.println(warblegarblewarble.garble());
    }

    /* Without overloading */
    public void castMagicSpellOnWizard(Wizard wizard) {
        System.out.println("You smite the wizard!");
        System.out.println("The wizard says " + wizard.getFinalWords());
    }

    public void castMagicSpellOnWitch(Witch witch) {
        System.out.println("You transmute the witch into a butterfly!");
        System.out.println("She curses you! " + witch.getRetalationCurse());
    }

    public void castMagicSpellOnWarlock(Warlock warlock) {
        System.out.println("You banish the warlock to another realm");
        System.out.println(warlock.getFadingScream());
    }

    public void castMagicSpellOnWarblegarblewarble(Warblegarblewarble warblegarblewarble) {
        System.out.println("What is this thing? You cast a spell and nothing happened?");
        System.out.println(warblegarblewarble.garble());
    }

    /* Without overloading our code becomes more redundant to read and more difficult to work with! And remember!
     * Overloaded methods may have the same names, but that doesn't mean they have the same implementation!  */
}
