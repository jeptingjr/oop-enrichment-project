package com.enrichment.D.abstract_inheritance;

/* Highlight all the code below this and Press CTRL + / to uncomment all the lines! you shouldn't see any compiler
 * errors, if you see any errors take the time to fix them before continuing! */

//public class IceCreamSundae extends Dessert {
//    /* The IceCreamSundae implementation is incomplete! This is why we are getting a compiler error. Hover over the red, and
//     * lets look at what IntelliJ is telling us:
//     * Class 'IceCreamSundae' must either be declared abstract or implement abstract method 'displayHowToEat()' in 'Dessert'
//     *
//     * This error essentially has two fixes. We could declare IceCreamSundae as abstract, which would mean another child
//     * class needs to extend IceCreamSundae and implement the method stubs from Dessert. However this would prevent us
//     * from instantiating an IceCreamSundae object, because just like interfaces, abstract classes can't be instantiated
//     *
//     * So instead we should take option #2, lets implement the method stubs here!
//     * Enrichment Exercise:
//     * 1. Look at Dessert.java and think about what methods we should implement here
//     * 2. Implement the methods in a way appropriate for an ice cream sundae
//     * 3. Add any additional methods that apply only to ice cream sundaes, and not all desserts in general
//     * 4. Return to AbstractStartHereForInheritance.java for the next steps
//     * */
//}

