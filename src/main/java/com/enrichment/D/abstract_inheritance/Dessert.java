package com.enrichment.D.abstract_inheritance;

public abstract class Dessert {
    /* Since Dessert is an abstract class we can define variables. You can't do this in an interface! */
    private int temperature;

    /* Abstract classes can also define specific implementations as well! Interfaces can't do this either! */
    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
    public boolean isCold() {
        if(temperature <= 32) {
            return true;
        }
        return false;
    }

    /* Here is what separates abstract classes from normal classes, we can stub out methods here just like with an
     * Interface. This is used the same way interface function stubs are used, we define them to express how implementing
     * classes should behave, but leave the details of the behavior up to the implementing classes */
    public abstract void displayHowToEat();
    public abstract String getDessertName();

    /* Next open ChocolateLavaCake.java and take a look at how we extend an abstract class */
}
