package com.enrichment.D.abstract_inheritance;

public final class ChocolateLavaCake extends Dessert {

    /* Notice how we only override the method stubs in our abstract class!
     *  Enrichment Exercise:
     *  1. Consider any additional functions or variables we could add to this class that makes sense for a
     *     ChocolateLava cake, but not other desserts.
     *  2. Consider any additional functions we could add to the Desert class that would make sense for all desserts
     *  3. Add your ideas to this file and Desert.java!
     *  4. Think about why ChocolateLavaCake.java doesn't have a constructor present
     *
     *  Next check out IceCreamSundae.java!
     * */
    @Override
    public void displayHowToEat() {
        System.out.println("Grab a fork and dig in! (maybe grab a napkin too so you don't get messy)");
    }

    @Override
    public String getDessertName() {
        return "Chocolate L A V A Cake!!";
    }
}
