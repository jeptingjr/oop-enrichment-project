package com.enrichment.D.abstract_inheritance;

public class StartHereForAbstractInheritance {
    public static void main(String[] args) {
        /* What if we needed the functionality of both a parent class and an interface? This is what abstract classes
         * are for, they provide the benefits of both a parent class and an interface all in one place!
         * Check out Dessert.java to take a look at an abstract class */

        /* This next section is to be completed once the comments instruct you to return here! */

        /* Enrichment Exercise:
        *  1. Read the code below and try to figure out what the console output would look like
        *  2. Identify which method calls are inherited from the Desert abstract class
        *  3. Identify which method calls are unique to the ChocolateLavaCake class
        *  4. Write some code below that uses your IceCreamSundae implementation and any custom methods added (get creative!)*/
        ChocolateLavaCake chocolateLavaCake = new ChocolateLavaCake();
        chocolateLavaCake.setTemperature(200);
        System.out.println("Is the " + chocolateLavaCake.getDessertName() + " cold? ");
        if(chocolateLavaCake.isCold()) {
            System.out.println("Yes it is! We need to heat it up!!");
        } else {
            System.out.println("Nope it is nice and hot here is how to eat it: ");
            chocolateLavaCake.displayHowToEat();
        }

        /*
         * Extending a parent class vs implementing an interface vs extending an abstract class:
         * If your class needs to inherit functionality already implemented, extend a parent class
         * If your class has a unique behavior that can be generalized across other classes, implement an interface
         * If you want to do both of the above at the same time, extend an abstract class
         *
         * That's all folks! */

    }
}
