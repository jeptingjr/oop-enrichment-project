package com.enrichment.B.inheritance;

public class StartHereForInheritance {
    public static void main(String[] args) {
     Fruit myFruit = new Fruit();
    /* Here we just created a new Fruit object
     * If you look at Fruit.java you should see an empty Constructor.
     * If you delete the constructor in Fruit.java we won't get any compiler errors.
     * This is because the default constructor (A constructor with no parameters) is created by Java
     * automatically. This is why we can write classes with no constructors, Java will make one for us!
     *
     * Enrichment Exercise:
     * 1. Create a constructor with arguments that initializes all private variables in Fruit.java
     * 2. Delete the default constructor
     * 3. Observe the compiler error in this main file
     * 4. Update line 5 of this file to use the new constructor you added, you can add any values here.
     * 5. Consider why the compiler error is gone
     * 6. Follow the instructions of the comments!                                                                  */

     /* Next, let's navigate to the Apple.java class */

    }
}
