package com.enrichment.B.inheritance;

public class Banana extends Fruit {

    /* Highlight all the code below this and Press CTRL + / to uncomment all the lines! you shouldn't see any compiler
     * errors, if you see any errors take the time to fix them before continuing! */

    /* Look at the two functions in this class, what are the differences?  */

//    public Banana(String color, boolean isSweet, boolean isTart) {
//        super(color, isSweet, isTart);
//    }
//
//    public String otherGetColor() {
//        return super.getColor();
//    }
//
//    @Override
//    public String getColor() {
//        return "Red";
//    }

    /* Copy and paste the code below into StartHereForInheritance.java, what will the output look like when the code runs?
     * When you run the code, come back to this file and follow the comments!

        Banana myBanana = new Banana("Yellow", true, false);
        System.out.println("Hello I'm a banana and my color is " + myBanana.getColor());
        System.out.println("Hello I'm a banana and my color is " + myBanana.otherGetColor());

    */

    /* What console output did you see? Analyze the two functions again and consider why we see two different colors
     * Moving forward, what happens if we delete the @Override getColor() function? (line 18-21) Do we get a compiler
     * error? What happens when we run InheritanceMain again? */

    /* The reason we first see two different outputs is because we are overriding Fruit.java's getColor method, while the
     * otherGetColor() method references the parent classes implementation via super. Regardless of what we override
     * we can always access the parent class' implementation via super. We can remove the @Override function because any
     * uses of Banana.getColor() will just call what is already implemented in Fruit.java. Remember! The @Override
     * annotation isn't mandatory, but it helps other developers understand what you are doing in your code so remember
     * to add it everytime you override!
     *
     * Overrides allow us to pick and choose what functionality we inherit from the parent class
     * super allows us to call the original implementations from the parent class
     *
     * If anything is still confusing keep playing with these class files, remember to always investigate and fix any
     * compiler errors you see!!
     *
     * That is all for inheritance!*/
}