package com.enrichment.B.inheritance;

// In English: Our public class Apple is a Fruit!
public class Apple extends Fruit {
    /* Check out this compiler error: There is no default constructor available in 'com.enrichment.inheritance.Fruit'
     * What happens if we write a default constructor for Apple? If we do that, we will see the compiler error move from
     * line 3 of our class, down to the default constructor. Go ahead and give this a try, see what happens.
     *
     * This compiler error is a bit ambiguous. It is complaining about not providing a default constructor, but it is
     * right there! The issue is not the lack of a default constructor, we need to actually call super() inside of the
     * Apple() constructor to get rid of this error.
     *
     * Try it out! If you get any compiler errors, hover over the red line to see what went wrong.
     * (if you get stuck, check out Orange.java!)
     *
     * Remember, super() is the super constructor, and is REQUIRED by child object constructor so Java can understand
     * how to construct the inherited parts of the parent object. In this example, super() tells Java "Before we finish
     * creating the Apple instance, we need to add whatever is inherited from Fruit to Apple"
     *
     * After finishing up here, open Orange.java!
     */
}
