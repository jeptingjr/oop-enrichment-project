package com.enrichment.B.inheritance;

public class Orange extends Fruit {

    /* Highlight all the code below this and Press CTRL + / to uncomment all the lines! you shouldn't see any compiler
     * errors, if you see any errors take the time to fix them before continuing! */

//    public Orange(String color, boolean isSweet, boolean isTart) {
//        super(color, isSweet, isTart);
//    }
//
//    public String getColor() {
//        return color; // Check out this compiler error, what do you think it means?
//    }

    /* Remember when using inheritance any private variables or methods are NOT inherited
     If we wanted to retrieve the value inside of the color variable WITHOUT changing color to public what would we need
     to do? Go ahead and make this change and then head over to Banana.java. If you're not sure about what you need to
     change Banana.java has a hint! */
}
