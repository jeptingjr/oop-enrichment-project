package com.enrichment.B.inheritance;

public class Fruit {
    private String color;
    private boolean isSweet;
    private boolean isTart;

    Fruit() {
        // This is default constructor!
        // Java will create this if no constructors are found
        // In other words, you can delete this! and you won't see any compiler errors in StartHereForInheritance.java
    }
}
