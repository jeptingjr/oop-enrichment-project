package com.enrichment.A.encapsulation;

public class SecondEncapsulationMain {
    public static void main(String[] args) {
        System.out.println("::::::::::::::::: Encapsulation Example #2 :::::::::::::::::");
        MoneyClass myMoneyClass = new MoneyClass();

        // Which is better?
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        // Or
        myMoneyClass.displayCoins();
        System.out.println();

        /* Let's say 499 of your fellow developers all implement myMoneyClass.dollars * 100 to convert our money
         * to pennies. But the requirements change all of a sudden! Now you must display how many quarters you have.
         * With encapsulation, we can just change MoneyClass.java to reflect this. WITHOUT encapsulation we would have
         * to update all 499 instances of "We have: " + myMoneyClass.dollars * 100 + " pennies". This would be terrible!
         *
         * Would you rather, update all of these print statements?
         * ExampleClass.java line 48: System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
         * OtherExampleClass.java line 92: System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
         * OtherExampleClass.java line 122: System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
         * AnotherExampleClass.java line 34: System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
         * [Imagine 495 more lines like this...]
         * Or would you just want to update line 7 in MoneyClass.java.
         * Encapsulation allows us to update 1 line, compared to 499 when used correctly.
         * Here is a more straightforward example */

        // Would you rather update these lines?
        System.out.println("All of these print lines must be updated one by one!");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println("We have: " + myMoneyClass.dollars * 100 + " pennies");
        System.out.println();
        // Or update one line in MoneyClass.java to update all of these lines?
        System.out.println("All of these print lines will be updated if MoneyClass.java is updated!");
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();
        myMoneyClass.displayCoins();

        /* Enrichment Exercise:
         * Give it a try! Change the displayCoins() function in MoneyClass.java to display how many quarters we have.
         * Give the code a run, and see how one edit updated all 10 of those function calls. */

        /* Encapsulation in a nutshell: It saves you from other developers making unwanted changes, and tedious updates.
         * That wraps up encapsulation! */
    }
}
