package com.enrichment.A.encapsulation;

public class MoneyClass {
    public int dollars = 1;

    public void displayCoins() {
        System.out.println("We have: " + dollars * 100 + " pennies");
    }
}
