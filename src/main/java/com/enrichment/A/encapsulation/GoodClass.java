package com.enrichment.A.encapsulation;

public class GoodClass {
    private String description = "This class is pretty good";

    public String getDescription() {
        return description;
    }
}
