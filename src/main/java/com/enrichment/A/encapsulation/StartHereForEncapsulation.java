package com.enrichment.A.encapsulation;

public class StartHereForEncapsulation {
    public static void main(String[] args) {

        /* This code showcases why encapsulation is important.
         * Encapsulation is defined as a way to restrict the direct access to some components of an object, so users
         * cannot access state values for all of the variables of a particular object
         * In other words, encapsulation allows us to better control what happens to our objects */

        System.out.println("::::::::::::::::: Encapsulation Example #1 :::::::::::::::::");
        // This bad class does NOT use encapsulation
        BadClass myBadClass = new BadClass();
        System.out.println("BadClass description: " + myBadClass.description);
        // Since BadClass ISN'T using encapsulation, we can change it's properties
        myBadClass.description = "This class is actually good!!";
        System.out.println("BadClass description: " + myBadClass.description);

        /* Run this main class and observe the console output.
         * Should a class named BadClass have a description that says it is good?
         * Should developers have the ability to modify this class and provide a confusing description? */

        /* Considering the above, is there a way to break the design of GoodClass without modifying its file (GoodClass.java)?
         * We can't add a confusing description to GoodClass, because it uses encapsulation! */
        GoodClass myGoodClass = new GoodClass();
        System.out.println("GoodClass description: " + myGoodClass.getDescription());
        System.out.println();

        /* Enrichment Exercise:
         * 1. Identify all the differences between GoodClass.java and BadClass.java
         * 2. What happens if the description variable in BadClass.java becomes private instead of public? (read the compiler errors!)
         * 3. What would we need to change in BadClass.java so that it uses encapsulation?
         * 4. What function could we implement in GoodClass.java to support setting the description to what we want?
         * 5. What would we need to change in GoodClass.java so once the description is set, no one can read it?
         *
         * Think of encapsulation as a tool to protect yourself from other developers. For example if you are working for
         * a company with 500 developers, using encapsulation is a way to tell the other 499 developers how your class is meant
         * to be used. This helps prevent bugs because any code used incorrectly is much more prone to bugs. This also
         * allows you to make updates to your class without interfering with the developers who are using your class.
         *
         * Let's explore this scenario in SecondEncapsulationMain.java! */
    }
}
