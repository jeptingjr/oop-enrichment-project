Hello! This is a simple Object Oriented Programming (OOP) Enrichment project! The point the project is to tinker with
the various concepts of Java in a more concise scope to transform knowledge into understanding and enhance student's
comfort with these topics.

Please keep these things in mind:

1. Compiler errors (the red lines) can help you figure out what went wrong. This project will intentionally cause compiler
   errors, so don't freak out when you run into some!

2. There are comments to guide you through this exercise please read them carefully and follow along!

3. Each topic package (ie: A.encapsulation) has a StartHere class, this is where the enrichment activities will begin.

4. While going through this project, take the time to read every bit of code in the example classes as well as any code
   you write. The goal is understanding, and a precursor to understanding is familiarity!

5. If your project gets too messy you can always delete the project folder and re-clone the project from Gitlab

6. Remember, if you have 1 compiler error anywhere in your project you can't run anything. Make sure your code is free of
   any red before you try running a main method

7. Block comments (/* */) will always contain guiding text for this project, single line comments (//) are for code examples

If you're ready to start open up the com.enrichment package and choose a topic you'd like to better understand.