package com.enrichment.F.access_modifiers.social_circle;

public class Friend {
    private You you;

    private String friendSecret;

    /* Since our Friend class is in the same package as our You class, Friend can have access to our personal secrets. */
    public void keepSecret() {
        this.friendSecret = you.getPersonalSecret();
    }
}
