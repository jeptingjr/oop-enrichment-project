package com.enrichment.F.access_modifiers.social_circle;

public class Family {
    private You you;

    private String familySecret;

    /* Since our Family class is in the same package as our You class, Friend can have access to our personal secrets.
     * Let's hop over to Stranger.java to see what happens when a class does not have access to our implementation */
    public void keepSecret() {
        this.familySecret = you.getPersonalSecret();
    }
}
