package com.enrichment.F.access_modifiers.social_circle;

public class You {
    /* This class is a super simple representation of you. Take a look at the access modifiers for each variable and each
     * function, see anything weird? */
    private String personalSecrets;

    private String socialSecurityNumber;

    /* Where is the access modifier for this function? When an access modifier isn't provided, java sets the access level
     * to default. This means only classes in the same package can access the function or variable. */
    String getPersonalSecret() {
        return personalSecrets;
    }

    /* The protected access modifier tells java that only child classes and classes within the same package can access
     * a function or variable*/
    protected String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /* Next check out Friend.java and Family.java to see access modifiers in action! */


}
