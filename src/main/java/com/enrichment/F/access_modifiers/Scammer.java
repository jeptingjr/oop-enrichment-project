package com.enrichment.F.access_modifiers;

import com.enrichment.F.access_modifiers.social_circle.You;

public class Scammer extends You {
/* Oh no our identity has been stolen! The Scammer is extending You so it can attempt to be you! */
    private String stolenSocialSecurityNumber;
    private String stolenSecret;
    /* Look the Scammer has unrestricted access to our social security number!! */
    public void commitIdentityTheft() {
        /* Remember, this super call references what is written in You.java */
        this.stolenSocialSecurityNumber = super.getSocialSecurityNumber();
        System.out.println("YOUR IDENTITY HAS BEEN STOLEN!! HAHAAAAAAAA!!");
    }

    /* What about our personal secret though? Finances can be recovered, but embarrassment will last forever!
     * Uncomment the code below, and see what happens */

//    public void stealSecretToo() {
//        this.stolenSecret = super.getPersonalSecret();
//    }

    /* What does the compiler error say? Why do we get this error? Our secret is safe since getPersonalSecret() is using
     * the default access modifier, which means only classes within the package social_circle can get our secret!
     *
     * Enrichment Exercise:
     * 1. Create a new class named SocialEngineer inside of the social_circle package
     * 2. Have the SocialEngineer steal both the social security number and secret from our You class
     * 3. Consider if the SocialEngineer needs to extend You in order to get the social security number
     * 4. After implementing the SocialEngineer, if you extended You, remove it, if you didn't extend You extend it!
     *    What happens? Why is this happening?
     * 5. Think about how restricting access changes how we write code, but does not influence the functionality during
     *    runtime
     * 6. Can our Friend class and Family class get our social security number? Why or Why not?
     *
     * Remember! access modifiers ARE NOT related to cyber security, they merely dictate how code that uses our classes
     * must be written. A private or protected string does nothing to keep credentials safe! In fact they do the exact
     * opposite by providing attackers a very simple attack vector to commit cyber crime.
     *
     * This concludes access modifiers, stay safe out there!
     * */
}
