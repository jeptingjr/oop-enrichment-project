package com.enrichment.F.access_modifiers;

import com.enrichment.F.access_modifiers.social_circle.You;

import java.util.List;

public class Stranger {
    private You you;
    List<String> informationList;

    /* Let's say you met a stranger, who wants to know way too much information about you. Uncomment the code below to
     * "see" what happens when we have a conversation with the invasive stranger */
    public void awkwardConversation() {
//        informationList.add(you.getPersonalSecret());
//        informationList.add(you.getSocialSecurityNumber());
    }

    /* Read the compiler errors! Since the Stranger class isn't in the social_circle package, it can't access your
     * personal information! However this isn't bullet proof, comment out the code above then continue on to Scammer.java */
}
