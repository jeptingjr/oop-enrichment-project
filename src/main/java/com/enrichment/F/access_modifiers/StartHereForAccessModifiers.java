package com.enrichment.F.access_modifiers;

public class StartHereForAccessModifiers {
    /* Remember the key words private and public? Well there are two more keywords we can leverage to better define how
     * our code is meant to be used, and prevent other developers from interacting with our implementations! These types
     * of words are called access modifiers, and they do just that. They modify how our code is accessed throughout the
     * application Check out You.java to learn more! */
    public static void main(String[] args) {

    }
}
