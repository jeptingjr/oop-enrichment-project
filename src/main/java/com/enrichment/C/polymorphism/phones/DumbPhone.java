package com.enrichment.C.polymorphism.phones;

import com.enrichment.C.polymorphism.Phone;

public class DumbPhone implements Phone {
    @Override
    public void displayHowToCallANumber() {
        System.out.println("***** Instructions for a Dumb Phone *****");
        System.out.println("1. Flip open the phone");
        System.out.println("2. Press the green phone button to open the dialer");
        System.out.println("3. Dial the number you wish to call");
        System.out.println();
    }
}
