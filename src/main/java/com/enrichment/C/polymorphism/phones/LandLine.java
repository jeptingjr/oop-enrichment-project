package com.enrichment.C.polymorphism.phones;

import com.enrichment.C.polymorphism.Phone;

public class LandLine implements Phone {
    @Override
    public void displayHowToCallANumber() {
        System.out.println("***** Instructions for a Land Line Phone *****");
        System.out.println("1. Pick up the phone");
        System.out.println("2. Dial the number you wish to call");
        System.out.println();
    }
}
