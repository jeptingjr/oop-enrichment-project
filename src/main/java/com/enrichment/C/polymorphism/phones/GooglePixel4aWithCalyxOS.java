package com.enrichment.C.polymorphism.phones;

import com.enrichment.C.polymorphism.Phone;

public class GooglePixel4aWithCalyxOS implements Phone {

    @Override
    public void displayHowToCallANumber() {
        System.out.println("***** Instructions for a Google Pixel 4a with CalyxOS *****");
        System.out.println("1. Turn on the screen");
        System.out.println("2. Unlock the screen");
        System.out.println("3. Swipe up to display the application drawer");
        System.out.println("4. Scroll down until you see the 'Telephone' app");
        System.out.println("5. Tap the blue circle with a number pad in the bottom right hand corner");
        System.out.println("6. Dial the number you want to call");
        System.out.println();
    }
}
