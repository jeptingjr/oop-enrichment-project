package com.enrichment.C.polymorphism.phones;

import com.enrichment.C.polymorphism.Phone;

public class SmartPhone implements Phone { // Remember [I]nterfaces uses the [I]mplements keyword!!
    @Override
    public void displayHowToCallANumber() {
        System.out.println("***** Instructions for a Smart Phone *****");
        System.out.println("1. Turn on the screen");
        System.out.println("2. Unlock the screen");
        System.out.println("3. Tap the phone app");
        System.out.println("4. Dial the number you wish to call");
        System.out.println();
    }
}
