package com.enrichment.C.polymorphism;

public interface Phone {
    /* An interface can only define method stubs, you can't declare variables, write a constructor, or write
     * implementations for functions. Interfaces are for method stubs and method stubs alone!
     * Enrichment Exercise:
     * Consider what would happen if we changed displayHowToCallANumber from public to private. */
    public void displayHowToCallANumber(); // This is a method stub!
}
