package com.enrichment.C.polymorphism;

import com.enrichment.C.polymorphism.phones.DumbPhone;
import com.enrichment.C.polymorphism.phones.GooglePixel4aWithCalyxOS;
import com.enrichment.C.polymorphism.phones.LandLine;
import com.enrichment.C.polymorphism.phones.SmartPhone;

import java.util.*;

public class StartHereForPolymorphism {
    public static void main(String[] args) {
        /* Polymorphism may sound complex, but it is really simple, check out the example code below! */

        // Notice how all of these objects are of different types!
        SmartPhone mySmartPhone = new SmartPhone();
        DumbPhone myDumbPhone = new DumbPhone();
        LandLine myLandLinePhone = new LandLine();
        GooglePixel4aWithCalyxOS mySpecificPhone = new GooglePixel4aWithCalyxOS();

        /* If you open SmartPhone.java (or any of the files in the phones package) you can see it is implementing the
         * phone interface, interfaces allow us to use polymorphism, consider the example below */

        // Let's say we have a list of Strings
        List<String> exampleList = new ArrayList<>();

        // Can we add an integer to this list? Like below? (If you're unsure uncomment the following line!)
        // exampleList.add(20);

        // We can't add an integer to exampleList, because that list was declared to only hold Strings!
        // Now consider this, what would happen if we uncomment line 32?
        List<Phone> phoneList = new ArrayList<>();
        // phoneList.add(mySmartPhone);

        /* Why aren't we getting a compiler error? The answer is polymorphism! Since the SmartPhone class implements
         * The Phone interface any SmartPhone Object can become a phone. Note that this does NOT go the other way! We
         * can't instantiate a Phone interface, and add it to a List<SmartPhone>. This is because you can't instantiate
         * interfaces like Phone! Speaking of which, open up Phone.java, take a look inside, and return here after
         * reading the comments! */

        // Lets add all of our phones to our list
        phoneList.add(myDumbPhone);
        phoneList.add(myLandLinePhone);
        phoneList.add(mySpecificPhone);

        // Now lets get an explanation of how to call a number from each phone
        for(Phone phone : phoneList) {
            // Since all of our phone classes implement Phone, they all have this function
            phone.displayHowToCallANumber(); // Give this file a run, and analyze the output!
            System.out.println();
        }

        /* What value does this provide? If you look through each of the classes in the phone package, you can see
         * that they each have their own unique implementation of displayHowToCallANumber(). Interfaces allow us to
         * take these unique implementations and treat them generically. Consider this, how would we display how to
         * call for each type of phone WITHOUT an interface or polymorphism? It could look something like this...

        mySmartPhone.displayHowToCallANumber();
        myDumbPhone.displayHowToCallANumber();
        myLandLinePhone.displayHowToCallANumber();
        mySpecificPhone.displayHowToCallANumber();

        * Or even WORSE it could look something like this!

        mySmartPhone.displayHowToCallOnASmartPhone();
        myDumbPhone.howDoIUseADumbPhone();
        myLandLinePhone.landlineCall();
        mySpecificPhone.phoneCallSteps();

         * We've not only lost the ability to use a loop to repeat our work for us, but we have also created more
         * work for ourselves. Imagine if we had 200 types of phones, would you rather write out all 200
         * phone.displayHowToCallANumber() calls, or use a loop? In the second block of code, we can also see another
         * huge issue if we don't use an interface, the functions that all do something similar are named differently.
         * This would create a nightmare for making updates, doing maintenance, and fixing bugs in the future. With
         * an interface, we can better organize our code so similar behaviors are tied to one method name, which makes
         * life easier for both you and any developers you are working with!
         *
         * Polymorphism also applies to child classes as well! If a child extends a parent, it can be added to a list
         * that holds it's parent's type!
         *
         * Enrichment Exercise:
         * 1. Create a new phone class called AlienPhone that implements the Phone interface
         * 2. Take note of any compiler errors you see, how can you resolve them?
         * 3. Add your AlienPhone to the phoneList list above
         * 4. Give the code a run, and see how the preexisting code handles your new class with no worries!
         * 5. Now make a modification to any of the displayHowToCallANumber implementations
         * 6. Run the code again, and notice how we didn't have to modify StartHereForPolymorphismMain to see our change
         *    in the console output!
         *
         * Extending a parent class vs implementing an interface:
         * If your class needs to inherit functionality already implemented, extend a parent class
         * If your class has a unique behavior that can be generalized across other classes, implement an interface
         *
         * Both interface implementations, and child classes can use polymorphism!
         * Something to consider! What could we change in the inheritance package to use polymorphism with our Fruits?
         *
         * This marks the end of the polymorphism enrichment!
         * */
    }
}
