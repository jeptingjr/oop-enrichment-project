package com.enrichment.E.managing_inheritance;

public class PopIdol {

    /* In this example, our PopIdol class has the ability to both sing the lead part, and harmonize. Notice that we added
     * final to the method signature of singLeadPart(), this means that any class that extends PopIdol can't inherit
     * the method singLeadPart(). Next navigate to BackupSinger.java.  */
    public final void singLeadPart() {
        System.out.println("Off the Florida Keys, There's a place called Kokomo");
    }

    public void harmonize() {
        System.out.println("Aruba, Jamaica, ooh, I wanna take you to, Bermuda, Bahama, come on pretty mama");
    }

    public void displaySongName() {
        System.out.println("Kokomo by The Beach Boys");
    }
}
