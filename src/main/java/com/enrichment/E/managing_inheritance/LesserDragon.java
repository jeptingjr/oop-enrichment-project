package com.enrichment.E.managing_inheritance;

/* Enrichment Exercise:
 * 1. Uncomment the lines of code below (CTRL + /)
 * 2. Read the compiler error
 * 3. Comment the code again to get rid of the compiler error
 * 4. Consider what might happen if our class doesn't have the final keyword, but a method does!
 * Now navigate to PopIdol.java to see what happens if we add the final keyword to a method! */

// public class LesserDragon extends Dragon {
// }
