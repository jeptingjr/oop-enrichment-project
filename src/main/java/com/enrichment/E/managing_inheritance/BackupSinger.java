package com.enrichment.E.managing_inheritance;

public class BackupSinger extends PopIdol {
    /* Enrichment Exercise:
     * 1. What do you think will happen when you uncomment the code below?
     * 2. Go ahead and uncomment the code (CTRL + /)
     * 3. Read any compiler errors that pop up!
     * 4. See if the compiler error disappears if we remove the @Override annotation
     * 5. Considering the effects of the final keyword, think of a way we can get rid of this compiler error without
     *    modifying PopIdol.java
     *
     * Remember, Java overrides parent class methods if the method signature matches! The @Override annotation is more for
     * human eyes, and less for the actual mechanics of code! This is why the compiler error still exists despite us removing
     * @Override!
     *
     * Here, even though our BackupSinger is a PopIdol, they are currently on back up duty, so they cannot sing the lead
     * part! The final keyword attached to the parent class' singLeadPart() method enforces this! */

//    @Override
//    public void singLeadPart() {
//        System.out.println("That's where we wanna go, Way down in Kokomo");
//    }
//
//    @Override
//    public void harmonize() {
//        System.out.println("Key Largo, Montego, Baby why don't we go?");
//    }
}
