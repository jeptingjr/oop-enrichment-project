package com.enrichment.E.managing_inheritance;

import java.util.ArrayList;
import java.util.List;

public class StartHereForManagingInheritance {
    /* Inheriting from a regular Class, an Abstract Class, and implementing an Interface are great tools for making code
     * more flexible and reusable. These concepts also help communicate the intent of our design and how the objects we
     * create relate to each other. In addition to all of that, Java has some more tricks up it's sleeve to help
     * developers to express how your code should be used */

    public static void main(String[] args) {
        /* The first trick Java gives us is the final keyword check this out! */
        final int finalInteger = 20000;
        /* Uncomment the line below, and look at the compiler error */
        // finalInteger = 0;

        /* What if we used final with an object? */
        final List<String> finalList = new ArrayList<>();

        /* Uncomment the line below, and take note of what happens? */
        // finalList.add("Hello, I'd like to be added to the list!");

        /* Why don't we get a compiler error? Remember the "value" stored by object variables is its address in memory!
         * When final is used in object instantiation it is preventing anyone from assigning a new memory address
         * to our variable. However, we can still manipulate the state of the underlying object, so we can still add
         * elements to an array list or call setters on a custom made object and so on!
         * Uncomment the next line to see this behavior in action. */

        // finalList = new ArrayList<>();

        /* Final also can be added to class definitions as well! Check out Dragon.java to see what it does! */

        /* Enrichment Exercise:
         * 1. Create your own implementations that either extend Dragon or PopIdol
         * 2. Play around with all the locations the final keyword can be used
         * 3. Observe any compiler errors you see, consider why they occurred, and how you should fix them.
         * */



    }
}
