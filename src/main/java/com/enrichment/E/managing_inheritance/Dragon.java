package com.enrichment.E.managing_inheritance;

public final class Dragon {
   /* When the final keyword is used in a class definition, that tells Java that no one can extend our class. In this
    * example our Dragon is greedy, and only it wants to have the ability to hoard gold, so the final keyword essentially
    * prevents any lesser dragons from having the ability to hoard gold. Check out LesserDragon.java to see this in action */
    public void hoardMoreGold() {
        // pretend there is some very interesting code here
    }
}
